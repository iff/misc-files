#!/usr/bin/python3
import json
import os
import sys
import requests

print("Content-type: application/json")
print("")

file = '/home/iff/pay-respects.log'
f = open(file, "a")
f.write("--------------------\n")

adress = os.environ['REMOTE_ADDR']
content_length = os.environ['CONTENT_LENGTH']
f.write("IP: " + adress + "\n")
f.write("Content-Length: " + content_length + "\n")
if int(content_length) > 1000:
	print("Content is too long, what are you trying to do?")
	f.write("Error: Content too long\n")
	sys.exit(0)

file = '/home/iff/pay-respects.db'
lines = open(file, "r").readlines()

autorization = os.environ['HTTP_AUTHORIZATION']
key = autorization.split(" ")[1]
url = ""
if key == lines[0].strip():
	url = lines[1].strip()
	key = lines[2].strip()
else:
	print("Wrong key, check your url configuration")
	f.write("Error: Wrong key\n")
	sys.exit(0)

autorization = "Bearer " + key
body = sys.stdin.read(int(content_length))
f.write("Body: " + body + "\n")

headers = {
	'Authorization': autorization,
	'Content-Type': 'application/json',
}

response = requests.post(url, headers=headers, data=body)
f.write("Response: " + response.text + "\n")
print(response.text)
