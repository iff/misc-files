from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThread, pyqtSignal

from settings import Ui_settingsWindow
from about import Ui_aboutWindow

from keyboard import is_pressed
from pyautogui import locateCenterOnScreen
from os.path import join, dirname
from time import sleep
from win32api import PostMessage, keybd_event, MAKELONG
from win32gui import FindWindow, GetWindowRect
from win32con import WM_KEYDOWN, WM_KEYUP, KEYEVENTF_KEYUP, WM_LBUTTONDOWN, WM_LBUTTONUP, MK_LBUTTON, MK_LBUTTON


dirname = dirname(__file__)

locate = locateCenterOnScreen

VK_CODE = {
	'backspace':0x08,
	'tab':0x09,
	'clear':0x0C,
	'enter':0x0D,
	'shift':0x10,
	'ctrl':0x11,
	'alt':0x12,
	'pause':0x13,
	'caps_lock':0x14,
	'esc':0x1B,
	'spacebar':0x20,
	'page_up':0x21,
	'page_down':0x22,
	'end':0x23,
	'home':0x24,
	'left':0x25,
	'up':0x26,
	'right':0x27,
	'down':0x28,
	'select':0x29,
	'print':0x2A,
	'execute':0x2B,
	'print_screen':0x2C,
	'ins':0x2D,
	'del':0x2E,
	'help':0x2F,
	'0':0x30,
	'1':0x31,
	'2':0x32,
	'3':0x33,
	'4':0x34,
	'5':0x35,
	'6':0x36,
	'7':0x37,
	'8':0x38,
	'9':0x39,
	'a':0x41,
	'b':0x42,
	'c':0x43,
	'd':0x44,
	'e':0x45,
	'f':0x46,
	'g':0x47,
	'h':0x48,
	'i':0x49,
	'j':0x4A,
	'k':0x4B,
	'l':0x4C,
	'm':0x4D,
	'n':0x4E,
	'o':0x4F,
	'p':0x50,
	'q':0x51,
	'r':0x52,
	's':0x53,
	't':0x54,
	'u':0x55,
	'v':0x56,
	'w':0x57,
	'x':0x58,
	'y':0x59,
	'z':0x5A,
	'numpad_0':0x60,
	'numpad_1':0x61,
	'numpad_2':0x62,
	'numpad_3':0x63,
	'numpad_4':0x64,
	'numpad_5':0x65,
	'numpad_6':0x66,
	'numpad_7':0x67,
	'numpad_8':0x68,
	'numpad_9':0x69,
	'multiply_key':0x6A,
	'add_key':0x6B,
	'separator_key':0x6C,
	'subtract_key':0x6D,
	'decimal_key':0x6E,
	'divide_key':0x6F,
	'F1':0x70,
	'F2':0x71,
	'F3':0x72,
	'F4':0x73,
	'F5':0x74,
	'F6':0x75,
	'F7':0x76,
	'F8':0x77,
	'F9':0x78,
	'F10':0x79,
	'F11':0x7A,
	'F12':0x7B,
	'F13':0x7C,
	'F14':0x7D,
	'F15':0x7E,
	'F16':0x7F,
	'F17':0x80,
	'F18':0x81,
	'F19':0x82,
	'F20':0x83,
	'F21':0x84,
	'F22':0x85,
	'F23':0x86,
	'F24':0x87,
	'num_lock':0x90,
	'scroll_lock':0x91,
	'left_shift':0xA0,
	'right_shift ':0xA1,
	'left_control':0xA2,
	'right_control':0xA3,
	'left_menu':0xA4,
	'right_menu':0xA5,
	'browser_back':0xA6,
	'browser_forward':0xA7,
	'browser_refresh':0xA8,
	'browser_stop':0xA9,
	'browser_search':0xAA,
	'browser_favorites':0xAB,
	'browser_start_and_home':0xAC,
	'volume_mute':0xAD,
	'volume_Down':0xAE,
	'volume_up':0xAF,
	'next_track':0xB0,
	'previous_track':0xB1,
	'stop_media':0xB2,
	'play/pause_media':0xB3,
	'start_mail':0xB4,
	'select_media':0xB5,
	'start_application_1':0xB6,
	'start_application_2':0xB7,
	'attn_key':0xF6,
	'crsel_key':0xF7,
	'exsel_key':0xF8,
	'play_key':0xFA,
	'zoom_key':0xFB,
	'clear_key':0xFE,
	'+':0xBB,
	',':0xBC,
	'-':0xBD,
	'.':0xBE,
	'/':0xBF,
	'`':0xC0,
	';':0xBA,
	'[':0xDB,
	'\\':0xDC,
	']':0xDD,
	"'":0xDE
}

# _windowName = "---"
# _hwnd = FindWindow(None, _windowName)


_windowName = None
_hwnd = None
_windowRec = None

k = None
l = None



# Keyboard event definitions
def press(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYDOWN, VK_CODE[i], 0)
		PostMessage(_hwnd, WM_KEYUP, VK_CODE[i], 0)
		sleep(.05)

def pressX(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYDOWN, VK_CODE[i], 0)
		keybd_event(VK_CODE[i], 0,0,0)
		PostMessage(_hwnd, WM_KEYUP, VK_CODE[i], 0)
		keybd_event(VK_CODE[i],0 ,KEYEVENTF_KEYUP ,0)
		sleep(.05)

def hold(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYDOWN, VK_CODE[i], 0)

def holdX(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYDOWN, VK_CODE[i], 0)
		keybd_event(VK_CODE[i], 0,0,0)

def release(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYUP, VK_CODE[i], 0)

def releaseX(*args):
	for i in args:
		PostMessage(_hwnd, WM_KEYUP, VK_CODE[i], 0)
		keybd_event(VK_CODE[i],0 ,KEYEVENTF_KEYUP ,0)


# Mouse event definitions

def doClick(cx,cy):
	cy = cy
	long_position = MAKELONG(cx, cy)
	PostMessage(_hwnd, WM_LBUTTONDOWN, MK_LBUTTON, long_position)
	PostMessage(_hwnd, WM_LBUTTONUP, MK_LBUTTON, long_position)

def cursorPosition(x, y):
	x = cursorPosition[0] - _windowRec[0]
	y = cursorPosition[1] - _windowRec[1] - 25 # Correction for Window's title bar

class useTicketsThread(QThread):
	finished = pyqtSignal(int)
	done = pyqtSignal()

	def __init__(self, repeat):
		QThread.__init__(self)
		self.repeat = repeat

		def __del__(self):
			self.wait()

		def _thread(self):
			press("enter")
			sleep(0.1)
			press("enter")
			sleep(0.2)
			press("left")
			press("enter")
			sleep(0.4)

		def run(self):
			if self.repeat == 0:
				while True:
					i = 0
					self._thread()
					self.finished.emit(i)
			else:
				for i in range(self.repeat):
					self._thread()
					self.finished.emit(i)
				self.done.emit()

class scratchThread(QThread):
	finished = pyqtSignal(int)
	done = pyqtSignal()

	def __init__(self, repeat):
		QThread.__init__(self)
		self.repeat = repeat

		def __del__(self):
			self.wait()

		def _thread(self):
			press("enter")

		def run(self):
			if self.repeat == 0:
				while True:
					i = 0
					self._thread()
					self.finished.emit(i)
			else:
				for i in range(self.repeat):
					self._thread()
					self.finished.emit(i)
				self.done.emit()

class grinderThread(QThread):
	finished = pyqtSignal(int)
	done = pyqtSignal()

	def __init__(self, repeat):
		QThread.__init__(self)
		self.repeat = repeat

	def __del__(self):
		self.wait()

	def _thread(self):
		press(k)
		sleep(0.7)
		press("enter")
		sleep(0.5)
		press("left")
		press("enter")
		sleep(0.1)
		press("enter")
		sleep(0.1)
		press("enter")
		sleep(0.3)
		press("esc")
		sleep(0.3)
		press("esc")
		sleep(0.3)
		press("esc")
		sleep(0.3)
		press(l)
		sleep(0.3)
		press("shift")
		sleep(0.1)
		press("enter")
		press("enter")
		press("down")
		press("enter")
		press("esc")
		sleep(0.2)
		press("esc")
		sleep(0.2)

	def run(self):
		if self.repeat == 0:
			while True:
				i = 0
				self._thread()
				self.finished.emit(i)
		else:
			for i in range(self.repeat):
				self._thread()
				self.finished.emit(i)
			self.done.emit()

class myRoomThread(QThread):
	finished = pyqtSignal(int)
	done = pyqtSignal()

	def __init__(self, repeat):
		QThread.__init__(self)
		self.repeat = repeat
		self.firstTime = True
		self.image = None


		def __del__(self):
			self.wait()

		def _thread(self):
			press(k)
			sleep(0.7)
			press("enter")
			sleep(0.5)
			press("down")
			press("down")
			press("left")
			press("enter")
			press("enter")
			sleep(0.1)
			press("enter")
			sleep(0.5)
			press("esc")
			sleep(0.3)
			press("esc")
			sleep(0.3)
			press("esc")
			sleep(0.3)
			press(l)
			sleep(0.3)
			if self.firstTime:
				while True:
					print(self.image)
					if self.image == None:
						self.image = locate(join(dirname,"data/txr/0000.txr"), grayscale = True, confidence=0.9)
					else:
						self.firstTime = False
						break
				doClick(self.image[0], self.image[1])
				sleep(0.1)
				press("down")
				press("enter")
				press("down")
				press("enter")
				sleep(0.3)
				press("right")
				press("enter")
				press("enter")
				sleep(0.1)
				press("esc")
				sleep(0.2)

		def run(self):
			if self.repeat == 0:
				while True:
					i = 0
					self._thread()
					self.finished.emit(i)
			else:
				for i in range(self.repeat):
					self._thread()
					self.finished.emit(i)
				self.done.emit()

class bluePerchThread(QThread):
	finished = pyqtSignal(int)
	done = pyqtSignal()

	def __init__(self,repeat):
		QThread.__init__(self)
		self.repeat = repeat

		def __del__(self):
			self.wait()

		def _thread(self):
			press("enter")
			sleep(0.3)
			press("down")
			press("enter")
			sleep(0.3)
			press("enter")
			press("up")
			press("up")
			press("enter")
			press("enter")
			press("down")
			sleep(0.1)
			press("down")
			press("enter")
			press("enter")
			sleep(0.3)
			press("right")
			press("enter")
			press("enter")
			sleep(0.3)
			press("esc")
			press("esc")
			sleep(0.5)
			press("esc")
			sleep(0.3)
			press("down")
			press("down")
			press("down")
			press("enter")
			sleep(0.5)
			for j in range(12):
				press("down")
				press("enter")
				sleep(0.3)
				press("left")
				press("enter")
				sleep(0.3)
				press("enter")
				sleep(0.5)
				press("enter")
				sleep(0.3)
				press("esc")
				sleep(0.3)
				press("up")
				press("up")
				press("up")

		def run(self):
			press(k)
			sleep(1)
			press("up")
			press("up")
			if self.repeat == 0:
				while True:
					i = 0
					self._thread()
					self.finished.emit(i)
			else:
				for i in range(self.repeat):
					self._thread()
					self.finished.emit(i)
				self.done.emit()

class Ui_MainWindow(object):

	def checkWindow(self):
		global k, _windowName, _hwnd

		_windowName = "---"
		_hwnd = FindWindow(None, _windowName)
		# _windowName = "*Untitled - Notepad"
		# _hwnd = FindWindow(None, _windowName)

		if _hwnd == 0:
			self.startButton.setEnabled(False)
			self.runningCheck.setText("--- not running")
		else:
			self.startButton.setEnabled(True)
			self.runningCheck.setText("--- running")
			_windowRec = GetWindowRect(_hwnd)


		def threadSelect(self, case, repeat):
			return {
				'0' : useTicketsThread(repeat),
				'1' : scratchThread(repeat),
				'2' : grinderThread(repeat),
				'3' : myRoomThread(repeat),
				'4' : bluePerchThread(repeat)
			}[case]

		def startThread(self, thread):
			data = open(join(dirname, "data/config.dat"), "r")
			line = data.readline()
			global k, l
			k = line[0]
			l = line[1]
			data.close()

			repeat = self.repeatAmountBox.value()
			self.progressBar.setMaximum(repeat)
			self.progressBar.setValue(0)

			self.thread = self.threadSelect(str(self.modeSelect.currentIndex()), repeat)
			self.thread.finished.connect(self.finished)
			self.thread.done.connect(self.done)

			self.thread.start()
			self.stopButton.setEnabled(True)
			self.stopButton.clicked.connect(self.thread.terminate)
			self.stopButton.clicked.connect(self.done)
			self.startButton.setEnabled(False)

		def finished(self, i):
			self.progressBar.setValue(int(i) + 1)

		def done(self):
			self.stopButton.setEnabled(False)
			self.startButton.setEnabled(True)
			self.progressBar.setMaximum(1)
			self.progressBar.setValue(0)

		def openSettings(self):
			self.window = QtWidgets.QMainWindow()
			self.ui = Ui_settingsWindow(dirname)
			self.ui.setupUi(self.window)
			self.window.show()

		def openAbout(self):
			self.window = QtWidgets.QMainWindow()
			self.ui = Ui_aboutWindow()
			self.ui.setupUi(self.window)
			self.window.show()

		def descriptionSelect(self, case):
			return {
				'0' : "---",
				'1' : "---",
				'2' : "---",
				'3' : "---",
				'4' : "---"
			}[case]

		def updateDescription(self):
			self.manual.setText("---" + self.descriptionSelect(str(self.modeSelect.currentIndex())))

		def setupUi(self, MainWindow):
			MainWindow.setObjectName("MainWindow")
			MainWindow.resize(750, 420)
			MainWindow.setMinimumSize(QtCore.QSize(750, 420))
			MainWindow.setMaximumSize(QtCore.QSize(750, 420))
			font = QtGui.QFont()
			font.setFamily("Microsoft YaHei")
			font.setPointSize(12)
			MainWindow.setFont(font)
			MainWindow.setWindowIcon(QtGui.QIcon('icon.ico'))
			MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
			self.centralwidget = QtWidgets.QWidget(MainWindow)
			self.centralwidget.setObjectName("centralwidget")
			self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
			self.horizontalLayout.setObjectName("horizontalLayout")
			self.leftArea = QtWidgets.QVBoxLayout()
			self.leftArea.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
			self.leftArea.setContentsMargins(0, -1, 0, -1)
			self.leftArea.setSpacing(6)
			self.leftArea.setObjectName("leftArea")
			self.verticalLayout = QtWidgets.QVBoxLayout()
			self.verticalLayout.setObjectName("verticalLayout")
			self.modeSelect = QtWidgets.QComboBox(self.centralwidget)
			self.modeSelect.setObjectName("modeSelect")
			self.modeSelect.addItem("")
			self.modeSelect.addItem("")
			self.modeSelect.addItem("")
			self.modeSelect.addItem("")
			self.modeSelect.addItem("")
			self.verticalLayout.addWidget(self.modeSelect)
			self.gridLayout = QtWidgets.QGridLayout()
			self.gridLayout.setObjectName("gridLayout")
			self.repeatAmountBox = QtWidgets.QSpinBox(self.centralwidget)
			self.repeatAmountBox.setMaximum(999999999)
			self.repeatAmountBox.setObjectName("repeatAmountBox")
			self.gridLayout.addWidget(self.repeatAmountBox, 1, 1, 1, 1)
			self.repeatAmountLabel = QtWidgets.QLabel(self.centralwidget)
			self.repeatAmountLabel.setObjectName("repeatAmountLabel")
			self.gridLayout.addWidget(self.repeatAmountLabel, 1, 0, 1, 1)
			self.verticalLayout.addLayout(self.gridLayout)
			self.line = QtWidgets.QFrame(self.centralwidget)
			self.line.setFrameShape(QtWidgets.QFrame.HLine)
			self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
			self.line.setObjectName("line")
			self.verticalLayout.addWidget(self.line)
			self.manual = QtWidgets.QLabel(self.centralwidget)
			self.manual.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
			self.modeSelect.currentIndexChanged.connect(self.updateDescription)
			self.manual.setWordWrap(True)
			self.manual.setObjectName("manual")
			self.verticalLayout.addWidget(self.manual)
			self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
			self.progressBar.setProperty("value", 0)
			self.progressBar.setObjectName("progressBar")
			self.verticalLayout.addWidget(self.progressBar)
			self.leftArea.addLayout(self.verticalLayout)
			self.horizontalLayout.addLayout(self.leftArea)
			self.rightArea = QtWidgets.QVBoxLayout()
			self.rightArea.setObjectName("rightArea")
			spacerItem = QtWidgets.QSpacerItem(200, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
			self.rightArea.addItem(spacerItem)
			self.runningCheck = QtWidgets.QLabel(self.centralwidget)
			self.runningCheck.setAlignment(QtCore.Qt.AlignCenter)
			self.runningCheck.setObjectName("runningCheck")
			self.rightArea.addWidget(self.runningCheck)
			self.runningCheckButton = QtWidgets.QPushButton(self.centralwidget)
			self.runningCheckButton.setObjectName("runningCheckButton")
			self.runningCheckButton.clicked.connect(self.checkWindow)
			self.rightArea.addWidget(self.runningCheckButton)
			self.optionsButton = QtWidgets.QPushButton(self.centralwidget)
			self.optionsButton.setObjectName("optionsButton")
			self.optionsButton.clicked.connect(self.openSettings)
			self.rightArea.addWidget(self.optionsButton)
			self.line_2 = QtWidgets.QFrame(self.centralwidget)
			self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
			self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
			self.line_2.setObjectName("line_2")
			self.rightArea.addWidget(self.line_2)
			self.startButton = QtWidgets.QPushButton(self.centralwidget)
			self.startButton.setObjectName("startButton")
			self.startButton.clicked.connect(self.startThread)
			self.rightArea.addWidget(self.startButton)
			self.stopButton = QtWidgets.QPushButton(self.centralwidget)
			self.stopButton.setObjectName("stopButton")
			self.stopButton.setEnabled(False)
			self.rightArea.addWidget(self.stopButton)
			self.horizontalLayout.addLayout(self.rightArea)
			MainWindow.setCentralWidget(self.centralwidget)
			self.menubar = QtWidgets.QMenuBar(MainWindow)
			self.menubar.setGeometry(QtCore.QRect(0, 0, 750, 27))
			self.menubar.setObjectName("menubar")
			self.help = QtWidgets.QMenu(self.menubar)
			self.help.setObjectName("help")
			MainWindow.setMenuBar(self.menubar)
			self.about = QtWidgets.QAction(MainWindow)
			self.about.setObjectName("about")
			self.help.addAction(self.about)
			self.menubar.addAction(self.help.menuAction())
			self.about.triggered.connect(self.openAbout)

			self.retranslateUi(MainWindow)
			QtCore.QMetaObject.connectSlotsByName(MainWindow)
			self.checkWindow()


		def retranslateUi(self, MainWindow):
			_translate = QtCore.QCoreApplication.translate
			MainWindow.setWindowTitle(_translate("MainWindow", "---"))
			self.modeSelect.setCurrentText(_translate("MainWindow", "---"))
			self.modeSelect.setItemText(0, _translate("MainWindow", "---"))
			self.modeSelect.setItemText(1, _translate("MainWindow", "---"))
			self.modeSelect.setItemText(2, _translate("MainWindow", "---"))
			self.modeSelect.setItemText(3, _translate("MainWindow", "---"))
			self.modeSelect.setItemText(4, _translate("MainWindow", "---"))
			self.repeatAmountLabel.setText(_translate("MainWindow", "---"))
			# self.manual.setText(_translate("MainWindow", "---"))
			self.updateDescription()
			self.runningCheck.setText(_translate("MainWindow", "---"))
			self.runningCheckButton.setText(_translate("MainWindow", "---"))
			self.optionsButton.setText(_translate("MainWindow", "---"))
			self.startButton.setText(_translate("MainWindow", "---"))
			self.stopButton.setText(_translate("MainWindow", "---"))
			self.help.setTitle(_translate("MainWindow", "---"))
			self.about.setText(_translate("MainWindow", "---"))


if __name__ == "__main__":
	import sys
	app = QtWidgets.QApplication(sys.argv)
	MainWindow = QtWidgets.QMainWindow()
	ui = Ui_MainWindow()
	ui.setupUi(MainWindow)
	MainWindow.show()
	sys.exit(app.exec_())
