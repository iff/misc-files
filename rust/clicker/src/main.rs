use inputbot::{KeybdKey::*, MouseButton::*};
use std::{thread::sleep, time::Duration};

fn main() {
	F1Key.bind(|| {
		while F1Key.is_pressed() {
			LeftButton.press();
			sleep(Duration::from_millis(10));
			LeftButton.release();
			sleep(Duration::from_millis(40));
		}
	});

	F2Key.bind(|| {
		while F2Key.is_pressed() {
			RightButton.press();
			sleep(Duration::from_millis(10));
			RightButton.release();
			sleep(Duration::from_millis(40));
		}
	});
	inputbot::handle_input_events();
}
